#!/bin/bash

# Check to make sure the CLI option is a valid one.
#
# TODO There's currently a bug where this filter will let multiple
# decimal points through. "0..1m" will slip through, for example.
# An "if" comparison using "[.]{0,1}" as part of the check doesn't
# appear to work. "[\.]{0,1}" doesn't work either.
if [[ $1 != "" ]]
then
    if [[ $1 =~ ^[0-9.]{1,5}+[smdh]{0,1}$ ]]
    then
        echo "Reboot will occur in $1 or when package upgrades and file"
        echo "synchronization has completed, whichever happens last."
    else
        echo "Usage: reboot.sh [NUMBER[SUFFIX]]"
        echo "If NUMBER is specified, induce a minimum wait time of"
        echo "NUMBER of seconds."
        echo "If SUFFIX is included, it changes the denomination."
        echo "s for seconds (default), m for minutes, h for hours, or d for days"
        exit 1
    fi
else
    echo "Reboot will occur when package upgrades and file"
    echo "synchronization has completed."
fi

# Prepare the variables and the log folder.
wait_for_pids=""
logtime=$(date +%Y-%m-%d_%H-%M-%S)
script_path=$(dirname "$(readlink -f "$0")")
mkdir -p $script_path/logs
echo "Reboot process initiated at $logtime."

# Begin upgrading packages.
nohup $script_path/sys-upgrade.sh > $script_path/logs/sys-upgrade_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Begin syncing user folder backups.
nohup $script_path/backup-files.sh > $script_path/logs/backup-files_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Begin first extra script.
nohup $script_path/extra-1.sh > $script_path/logs/extra-1_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Begin second extra script.
nohup $script_path/extra-2.sh > $script_path/logs/extra-2_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Wait for the user-specified amount of time if it was specified.
if [[ $1 != "" ]]
then
    sleep $1
fi

# Check to see if the package upgrades and backup sync processes are done.
wait $wait_for_pids

# Disconnect from the VPN if it's connected to avoid the startup bug
#if [[ $(nmcli c show | grep Proton) != "" ]]
#then
#    protonvpn-cli d
#fi

# Initiate reboot.
shutdown -r now
