#!/bin/bash

# Prepare the variables and the log folder.
wait_for_pids=""
logtime=$(date +%Y-%m-%d_%H-%M-%S)
script_path=$(dirname "$(readlink -f "$0")")
mkdir -p $script_path/logs
echo "Upgrade initiated at $logtime."

# Begin upgrading packages.
nohup $script_path/sys-upgrade.sh > $script_path/logs/sys-upgrade_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Check to see if the package upgrades and backup sync processes are done.
wait $wait_for_pids

sync
