#!/bin/bash

# Both apt and snap require root to work.
# This set updates both and removes leftover packages.
# Remove/comment out the the "apt autoremove" line if you
# prefer to keep leftover packages.
#
# TODO Add support for other package managers like pacman and yum.
pkexec /bin/bash <<EOF
    apt update
    apt upgrade -y
    apt autoremove --purge -y

    snap refresh
EOF

# Update Flatpak.
# Remove the second command if you would prefer to keep leftover packages.
#
# TODO Flatpak doesn't require root except for the first initialization.
# As a result, this needs a check to ensure it's already been initialized,
# and to skip the process if it's not detected.
flatpak update -y
flatpak remove --unused -y

# If you use Python and have pip or pipx installed, you can uncomment the
# respective line. If using pip, also customize the line to contain all
# your installed packages.
#
# TODO Modularize pip to store the packages in a separate file
# for easy cumstomization, as well as add a means to detect if pip
# is installed and used.
#pip install -U pip
#pipx upgrade-all