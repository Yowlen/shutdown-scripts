#!/bin/bash

# This file is empty by default. If you have any additional commands
# to run, such as for backing up important files, then you can add them
# here. This will run in parallel to the system upgrades, allowing
# both to run at the same time and save time that way.
