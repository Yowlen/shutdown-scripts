### Custom Shutdown Scripts

This set of scripts automatically upgrades packages and does a backup of important files.

It has separate `shutdown.sh` and `reboot.sh` scripts, though the plan is to eventually introduce better CLI argument parsing to allow for `shutdown.sh` to function using any combination of CLI arguments from both the `shutdown` `sleep` commands.

The scripts are currently optimized for Ubuntu-based distributions that use apt, snap, and flatpak, and do not contain any checks to ensure they're set up properly, so please look over the `sys-upgrade.sh` script to ensure it's set up to match your needs.

The `backup-files.sh` script is where to put any commands to back up important files. It's empty by default in the repo, so it will simply do nothing when it's called by the main scripts.