#!/bin/bash

# Prepare the variables and the log folder.
wait_for_pids=""
logtime=$(date +%Y-%m-%d_%H-%M-%S)
script_path=$(dirname "$(readlink -f "$0")")
mkdir -p $script_path/logs
echo "Backup initiated at $logtime."

# Begin syncing user folder backups.
nohup $script_path/backup-files.sh > $script_path/logs/backup-files_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Begin first extra script.
nohup $script_path/extra-1.sh > $script_path/logs/extra-1_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Begin second extra script.
nohup $script_path/extra-2.sh > $script_path/logs/extra-2_$logtime.log & wait_for_pids="$wait_for_pids $!"

# Check to see if the package upgrades and backup sync processes are done.
wait $wait_for_pids

sync
